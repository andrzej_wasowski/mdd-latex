% ver 0.46 (mainline hosted in mdd-latex)
\synctex=1
\RequirePackage[l2tabu, orthodox]{nag} % track deprecated packages

% this is still a big mess that needs to be cleaned up
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{mdd-slides}[2021/08/31 stylized beamer slides]

\newcommand{\BTR}{\ensuremath{\color{ClrDarkDominant}\blacktriangleright}}
\newcommand{\PTR}{\ensuremath{\color{ClrDarkDominant}\phantom{\blacktriangleright}}}

\newcommand*\MddSlidesModeOption{presentation}
\DeclareOption{handout}{%
  \renewcommand*\MddSlidesModeOption{handout}%
}
\DeclareOption{presentation}{}


% for simplicity, I don't follow the beamer style of key value pair (Ultimately this should be done with some keyvalue package, see http://tex.stackexchange.com/a/38953/63936, but I was too afraid that I will just arrive into issues with beamers kv mechanisms, so I kept down to simple)

\newcommand*\MddSlidesAspectRatioOption{aspectratio=43}
% only use in this class file (workaoround)
\newcommand*\MddPaperWidth{20cm}

\DeclareOption{aspectratio43}{%
  \renewcommand*\MddSlidesAspectRatioOption{aspectratio=43}
  \renewcommand*\MddPaperWidth{20cm}
}
\DeclareOption{aspectratio1610}{%
  \renewcommand*\MddSlidesAspectRatioOption{aspectratio=1610}
  \renewcommand*\MddPaperWidth{20cm}
}
\DeclareOption{aspectratio169}{%
  \renewcommand*\MddSlidesAspectRatioOption{aspectratio=169}
  \renewcommand*\MddPaperWidth{16.0cm}
}


\ProcessOptions

\LoadClass[\MddSlidesModeOption, \MddSlidesAspectRatioOption,
  xcolor=pdftex, xcolor=table, 10pt, ignorenonframetext]{beamer}

\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage[export]{adjustbox}

\RequirePackage{
  alltt,
  colortbl,
  helvet,
  % lastpage, problems in texlive-2023
  listings,
  mdd-tikz,
  microtype,
  pdfpages,
  underscore,
  xspace,
}

\renewcommand{\ttdefault}{txtt}

\RequirePackage[inference]{semantic}
\RequirePackage[pagewise]{lineno}
\RequirePackage[scale = 0.85]{noto-mono}

\setlength\linenumbersep{1mm}


\useinnertheme{rectangles}

\setbeamercolor*{titlelike}{
  fg = ClrDominant,
  bg = white
}

\defbeamertemplate*{headline}{empty}{\vskip0pt}
\defbeamertemplate*{footline}{empty}{\vskip0pt}

\defbeamertemplate*{footline}{Dagstuhl}{\vskip0pt}
\defbeamertemplate*{headline}{Dagstuhl}%
{
  \leavevmode%
  \hbox{%
  \begin{beamercolorbox}[wd=.2\paperwidth,ht=2.25ex,dp=1ex,left]%
	  {section in head/foot}%b
    \usebeamerfont{part in head/foot}\insertshortpart\hspace*{2ex}
  \end{beamercolorbox}%
  \begin{beamercolorbox}[wd=.8\paperwidth,ht=2.25ex,dp=1ex,left]%
	  {subsection in head/foot}%
    \usebeamerfont{section in head/foot}\hspace*{2ex}\insertsection
  \end{beamercolorbox}}%
  \vskip0pt%
}

\setbeamerfont{frametitle}{size=\LARGE}

\setbeamertemplate{headline}[empty]
\setbeamersize{text margin left=3mm}
\setbeamersize{text margin right=8mm}
\setbeamertemplate{footline}{%
  \rightline{\raisebox{1.5ex}{
      \usebeamercolor[fg]{title like}
      \scalebox{0.630}%
      {\copyright\ Andrzej W\k{a}sowski, IT\,University of Copenhagen}%
		  \hspace{1.33mm}
      \textbf{\insertframenumber}\hspace{2ex}}%
  }%
}

\setbeamercovered{invisible}
\setbeamertemplate{title page}[default][left]
\setbeamertemplate{blocks}[default]
\setbeamerfont*{block title example}{size=\small}
\setbeamerfont*{block body example}{size=\small}

% definitions, theorems, etc.
\setbeamercolor*{block title}{
  bg = ClrDominant!90,
  fg = white,
}
\setbeamercolor*{block body}{
  bg = ClrLightBackground, % not sure what this doesn
  fg = black
}

\setbeamertemplate{blocks}[default]

\setbeamerfont{title}{size=\LARGE}

\setbeamerfont{framesubtitle}{
  size = \normalsize,
  family = \sffamily,
	series = \normalfont
}

\setbeamerfont{author}{size=\normalsize}
\setbeamerfont{date}{size=\normalsize}
\setbeamertemplate{enumerate item}{\colorbox{ClrDominant!90}{\color{white}\small\insertenumlabel}}

\setbeamercolor*{itemize item}{fg = ClrDominant!90}
\setbeamercolor*{section in toc}{fg = ClrDominant!90}
\setbeamercolor*{itemize subitem}{fg = ClrDominant}
\setbeamercolor*{itemize subsubitem}{fg = ClrDominant}
\setbeamertemplate{itemize items}[square]
\setbeamerfont*{itemize items}{size=\Large}

\beamertemplatenavigationsymbolsempty

\setbeamertemplate{frametitle}{
	\vspace{1mm}
	\usebeamerfont{frametitle}\usebeamercolor{frametitle}%
	\hbox{\insertframetitle\strut}\par
	\usebeamerfont{framesubtitle}\color{ClrDarkDominant}%
		\ifx\insertframesubtitle\@empty\vspace{-5mm}\else%
		\insertframesubtitle\strut\vspace{-5mm}\par\fi}


\setbeamersize{
  text margin left = 1em,
  text margin right = 1em
}

\usefonttheme[onlylarge]{structurebold}
\usefonttheme[onlymath]{serif}

\lstset{%
	keywordstyle=\color{ClrDarkGrayDominant},
	stringstyle=\color{ClrDarkGrayDominant},
	commentstyle=\color{ClrDarkGrayDominant}\itshape\ttfamily,
	numbers=left,
	numberstyle={\sffamily\tiny\color{ClrDarkGrayDominant}},
	numbersep=3pt,
	showspaces=false,
	showstringspaces=false,
	escapechar=^,
  basicstyle = {%
    \lst@ifdisplaystyle
      \ttfamily\footnotesize
    \else
      \normalsize
    \fi\ttfamily},
	tabsize=2
}


\hypersetup{
  colorlinks,
  urlcolor = ClrDominant
}

\graphicspath{{fig/}}

% handle extensions for metapost (is this correct?) (do I still use it?)
\DeclareGraphicsRule{*}{mps}{*}{}
% handle extensions for inkscape export
\DeclareGraphicsRule{.pdftex}{pdf}{.pdftex}{}

% A legacy hack, commented out on 2021-08-31, checking what happens
% \pdfpageattr{/Group <</S /Transparency /I true /CS /DeviceRGB>>}

% use this macro to insert a PDF file containing an entire slide (first page)
% good for importing from powerpoint, inserting photographs, etc
\newcommand{\pdfslide}[2][1]{%
	\frame[c]{%
		\centering\noindent%
		\hbox{%
      \hspace{-3.5mm}%
      \includegraphics[page=#1,height=\paperheight]{#2}%
    }
	}
}

\newcommand{\mycite}[1]{%
  \tikz[
    overlay,
    remember picture
  ]
  \node[inner sep=0mm, outer
  sep=0mm, minimum height=1cm, anchor=south, text
  width=\textwidth, font=\scriptsize] [above=-1mm of current page.south] {{#1}};}

\newcommand{\singleItem}[1]{\begin{itemize}\item
			#1\end{itemize}\vspace{-0mm}}

% \tikz coordinate
\newcommand{\tc}[1]{
  \smash{\rlap{\tikz\node[
	  inner sep = 0mm,
	  outer sep = .0ex,
	  minimum height = .6 \baselineskip,
	  anchor = center,
    yshift = .7ex
  ](#1){\phantom{x}};}}}

\newcommand{\tC}[1]{#1\tikz\node(#1){};}

\setlength{\leftmargini}{0.8em}

% draws a line of an eastern edge of the node to make it better
% visible
\newcommand<>{\eflank}[1]{\draw#2[ClrDarkGrayDominant,solid,very thin]
	([xshift=-.5mm]#1.north east) -- ++(right:.5mm) |- ([xshift=-1.2mm]#1.south east)}
\newcommand<>{\wflank}[1]{\draw#2[ClrDarkGrayDominant,solid,very thin]
	([xshift=1.2mm]#1.north west) -- ++(left:1.2mm) |- ([xshift=0.5mm]#1.south west)}
\newcommand<>{\sflank}[1]{\draw#2[ClrDarkGrayDominant,solid,very thin]
	([yshift=1.2mm]#1.south west) -- ++(down:1.2mm) -| ([yshift=0.5mm]#1.south east)}
\newcommand<>{\nflank}[1]{\draw#2[ClrDarkGrayDominant,solid,very thin]
	([yshift=-0.5mm]#1.north west) -- ++(up:0.5mm) -| ([yshift=-1.2mm]#1.north east)}
\newcommand<>{\boxnode}[1]{\eflank{#1};\wflank{#1};\sflank{#1};\nflank{#1}}

% node title
% guess the color
% #1 node name (the title will attach to that node)
% #2 title label text
\newcommand<>{\nodetitle}[2]{\node#3[fancytitle] at (#1.north west) {#2}}






% so that transparency works on other backgrounds than white
%\setbeamercovered{transparent}

\newcommand{\titlinewidth}{44.7mm}
\newcommand{\titline}[1]{\leftline{\resizebox{\titlinewidth}{!}{\Large\textbf{\strut
        #1}}}\vspace{-2mm}\leavevmode}


\newcommand{\Lecture}[2]{\AtriumBackground
\begin{frame}[t,label=titleframe]

  \begin{tikzpicture}

    \node[
      SimpleGlass,
      text width = 1.1 \linewidth,
      color = ClrDominant,
      font = \bfseries,
      anchor = south west
    ] (title) at (2mm,-0.78 \paperheight)
    {%
      \titline{\strut\textnormal{\color{ClrDominant}\,@AndrzejWasowski@scholar.social}\hspace{28mm}}\\[1.2mm]
      \titline{\color{ClrDominant}Andrzej W\k{a}sowski}\\[-2 mm]
      \titline{\color{ClrDominant}Florian Biermann}\\[2 mm]
      \titline{Advanced}\\[-1mm]
      \titline{Programming}\\[2mm]
      \hrule\leavevmode\\[1mm]
      \color{ClrDarkDominant}
      \Large #1 #2\\[2mm]
      \color{ClrDominant}
      \hrule\leavevmode\\[2mm]
      };

    \node[
      SimpleGlass,
      anchor=south west,
      yshift=10mm
    ] at (2mm,-1.0\paperheight)
    {\includegraphics[height=7.02mm]{logo.png}\hspace{2mm}\includegraphics[height=7.02mm]{logo.pdf}};

  \end{tikzpicture}
\end{frame}}

\newcommand{\blankscreen}{}%
\renewcommand{\blankscreen}{%
\begingroup
\setbeamertemplate{navigation symbols}{}
\setbeamercolor{background canvas}{bg=black}
\begin{frame}\label{blank}\end{frame}
\endgroup}

\newcommand{\blackslogan}[2]{
\begingroup
\setbeamertemplate{navigation symbols}{}
\BlackBackground
\begin{frame}[c,label=#2]
	\begin{center}{\huge\textbf{\color{white}#1}}\end{center}
\end{frame}
\endgroup}

\newcommand{\whiteslogan}[2]{
\begingroup
\setbeamertemplate{navigation symbols}{}
\setbeamercolor{background canvas}{bg=white}
\begin{frame}[c,label=#2]
	\begin{center}{\huge\textbf{\color{ClrDominant}#1}}\end{center}
\end{frame}
\endgroup}

\newcommand{\ImgSloganWhiteCentered}[4][]{%
{\begingroup
  \setbeamertemplate{navigation symbols}{}%
  \SetBackground{#2}
  \begin{frame}[c,label=#4]
    \begin{center}{\huge\textbf{\usebeamercolor[fg]{title like}#3}}\end{center}
    \mycite{{\smash{\strut}\usebeamercolor[fg]{title like} #1}}
  \end{frame}}
\endgroup}

\newcommand {\ImgSloganWhiteTL} [4] [] {
  \begingroup
  \setbeamertemplate{navigation symbols}{}%
  \SetBackground{#2}
  \setbeamercolor{title like}{fg = white}
  \begin{frame}[t,label=#4]
    \huge
    \textbf{\usebeamercolor[fg]{title like}#3}%
    \mycite{{\strut\color{white}#1}}
  \end{frame}
  \endgroup
}

\newcommand {\ImgSloganDominantTL} [4] [] {
  \begingroup
  \setbeamertemplate{navigation symbols}{}%
  \SetBackground{#2}
  \begin{frame}[t,label=#4]
    \huge
    \textbf{\color{ClrDominant}#3}%
    \mycite{{\strut\color{ClrDominant}#1}}
  \end{frame}
  \endgroup
}

\newcommand {\ImgSloganBlackTL} [4] [] {
  \begingroup
  \setbeamertemplate{navigation symbols}{}%
  \SetBackground{#2}
  \begin{frame}[t,label=#4]
    \huge
    \textbf{\color{black}#3}%
    \mycite{{\strut\color{black}#1}}
  \end{frame}
  \endgroup
}
\newcommand {\ImgSloganDominantTR} [4] [] {
  \begingroup
  \setbeamertemplate{navigation symbols}{}%
  \SetBackground{#2}
  \begin{frame}[t, label=#4]
    \huge
    \strut\hfill\textbf{\color{ClrDominant}#3}%
    \mycite{{\strut\hfill\color{ClrDominant}#1}}
  \end{frame}
  \endgroup
}

\newcommand {\ImgSloganBlackTR} [4] [] {
  \begingroup
  \setbeamertemplate{navigation symbols}{}%
  \SetBackground{#2}
  \begin{frame}[t, label=#4]
    \huge
    \strut\hfill\textbf{\color{black}#3}%
    \mycite{{\strut\hfill\color{black}#1}}
  \end{frame}
  \endgroup
}

\newcommand{\himgslide}[1]{%
{%\begingroup
\setbeamercolor{background canvas}{bg=black}%
\setbeamertemplate{navigation symbols}{}%
\begin{frame}[c,plain,label=#1]{\himg{#1}}\end{frame}}}%\endgroup}}

\newcommand{\himg}[1]{\includegraphics[page=1,height=\paperheight,center]{#1}}
\newcommand{\wimg}[1]{\includegraphics[page=1,width=\paperwidth,center]{#1}}

\newcommand{\wimgslide}[1]{%
{\begingroup
\setbeamercolor{background canvas}{bg=black}%
\setbeamertemplate{navigation symbols}{}%
\begin{frame}[c,plain,label=#1]{\wimg{#1}}\end{frame}\endgroup}}

\newcommand{\wwimgslide}[1]{%
{\begingroup
%\setbeamercolor{background canvas}{bg=black}%
\setbeamertemplate{navigation symbols}{}%
\begin{frame}[c,plain,label=#1]{\wimg{#1}}\end{frame}\endgroup}}




\newcommand{\drobna}{\scriptsize}

\newcommand{\agenda}[2]{%
\AtriumBackground
\frame[t,label=#1]{%
	\begin{tikzpicture}
		\node at (0,0) {};
		\node[anchor=south west,inner sep=0]
      (agenda) at (0em,-0.9\textheight)
			{\includegraphics[scale=0.3]{\ThemeAgendaImg}};
		\node[ThemeBox,Framed, inner ysep=3mm, anchor=south west, text width=0.753\textwidth]
      (glass) at ([yshift=2ex]agenda.north west) {#2};
	\end{tikzpicture}
}}



% #1 title
% #2 subtitle
% #3 body
\newcommand{\ItemizedSlide}[3]{%
\WhiteBackground
\frame[b]{%
	\frametitle{#1}
	\framesubtitle{\color{ClrDarkDominant}#2}
	\begin{tikzpicture}
		\node[ThemeBox,anchor=south west] (glass)
			{\begin{minipage}{0.753\textwidth}{#3}\end{minipage}};
	\end{tikzpicture}
	\vspace{6mm}
}}




\newtheorem{exercise}[theorem]{Exercise}

\newcommand{\rframetitle}[1]{\rightline{\color{ClrDarkDominant!90}\textbf{\LARGE\strut#1}}}
\newcommand{\lframetitle}[1]{\leftline{\color{ClrDarkDominant!90}\textbf{\LARGE\strut#1}}}
\newcommand{\rframesubtitle}[1]{\rightline{\color{ClrDarkDominant}#1\textbf{\LARGE}}}
\newcommand{\lframesubtitle}[1]{\leftline{\color{ClrDarkDominant}#1\textbf{\LARGE}}}

\newcommand{\MathBackground}{%
	\setbeamertemplate{background}{%
	\includegraphics[scale=5.0,clip,trim=0 0 0 0mm]{math}}}

\newcommand{\BoeingBackground}{%
	\setbeamertemplate{background}{%
	\includegraphics[scale=.53,clip,trim=0 0 0 0mm]{747}}}

\newcommand{\CrowdBackground}{%
	\setbeamertemplate{background}{%
	\includegraphics[scale=.23,clip,trim=10mm 0 0 190mm]{crowd-lo}}}

\newcommand{\StadiumCrowdBackground}{%
	\setbeamertemplate{background}{%
	\includegraphics[scale=.57,clip,trim=0 0 0
  0mm]{stadium-crowd.jpg}}}


\newcommand{\PencilBackground}{%
  \setbeamercolor{background canvas}{bg=}
  \setbeamertemplate{background}{%
    \tikz[remember picture,overlay] {%
      \node[anchor=south east,inner sep=0mm,yshift=-10mm] at (current
      page.south east)
      {\includegraphics[scale=0.19,clip,trim=10cm 0 220mm 0]{fig/pencil-med.jpg}};
    }}}


\newcommand{\StudyBackground}{%
	\setbeamertemplate{background}{%
		\begin{tikzpicture}
			\node[SimpleGlass] at (0,-4.5)
			{\includegraphics[scale=.67]{exam.jpg}};
		\end{tikzpicture}}}
%	\rightline{\strut\raisebox{-99mm}{\includegraphics<2->[clip,trim=16cm 0
%			16cm 0,scale=0.09]{exam.jpg}\hspace{-22mm}}}}}


\newcommand{\AutomationBackground}{
	\setbeamertemplate{background}{%
	\rightline{\strut\raisebox{-99mm}{\includegraphics[clip,trim=10cm 0
			15cm 10mm,scale=0.072]{robot-claw-small}\hspace{-22mm}}}}}


\newcommand{\QuestionBackground}{
  \setbeamertemplate{background}{%
  \rightline{\includegraphics[clip,trim=1cm 0 3cm
    0,scale=.64]{guy-question-scaled}\hspace{-12mm}}}}

%	\rightline{\strut\raisebox{-99mm}{\includegraphics[clip,trim=16cm 0
%			16cm 0,scale=0.10]{guy-question-large}\hspace{-22mm}}}}}



\newcommand{\ArchitectureBackground}{%
	\setbeamertemplate{background}{%
	\includegraphics[scale=3.0,clip,trim=0 0 0 5mm]{architecture.png}}}

\newcommand{\TypesBackground}{%
	\setbeamertemplate{background}{%
		\includegraphics[scale=.2,center]{plugs-0}}}

\newcommand{\ProgLangBackground}{%
	\setbeamertemplate{background}{%
		\includegraphics[scale=.5,center]{program-0}}}

\newcommand{\BookBackground}{%
		\setbeamertemplate{background}{%
			\tikz[overlay]\node[anchor=north west,inner sep=0,shift={(-.1ex,.1ex)}]
			{\includegraphics[height=1.1\textheight]{newspaper-0}};}}

\newcommand{\SetBackground}[1]{%
\setbeamertemplate{background}{%
  \tikz[overlay,remember picture]\node[SimpleGlass]{~};%
	\tikz[overlay,remember picture]\node[outer sep=0mm, inner
  sep=0mm] [below=0mm of current page.north west, anchor=north west]
  {\includegraphics[width=\MddPaperWidth]{#1}};}}

%\adjustbox{min size={\paperwidth}{\paperheight}}%
%                   {\includegraphics{PM5644.jpg}}};

%			{\includegraphics[height=1.0\paperheight]{#1}};}}

\newcommand{\AtriumBackground}{%
		\setbeamertemplate{background}{%
			\tikz[overlay]\node[SimpleGlass,anchor=north
         east] at (1.03\paperwidth,0.05)
			{\includegraphics[height=1.0\textheight]{atrium-bw.png}};}}

\newcommand{\XtendBackground}{%
		\setbeamertemplate{background}{%
			\tikz[overlay]\node[SimpleGlass,shift={(0.5\textwidth,-0.5\textheight)},anchor=center]
			{\includegraphics[width=10.9cm]{xtend-logo-faint}};}}

\newcommand{\XtextBackground}{%
		\setbeamertemplate{background}{%
			\tikz[overlay]\node[SimpleGlass,shift={(0.5\textwidth,-0.5\textheight)},anchor=center]
			{\includegraphics[width=10.9cm]{xtext-logo-faint}};}}


\newcommand{\DSLBackground}{%
		\setbeamertemplate{background}{%
			\tikz[overlay]\node[SimpleGlass,shift={(0.5\textwidth,-0.5\textheight)},anchor=center]
			{\includegraphics[width=1.3\textwidth]{dentist-tools.png}};}}


\newcommand{\SinatraBackground}{%
		\setbeamertemplate{background}{%
			\tikz[overlay]\node[SimpleGlass,shift={(0.52\textwidth,-0.55\textheight)},anchor=center]
			{\includegraphics[width=1.1\textwidth,clip,trim=0 16cm 0 0]{legend.png}};}}







\newcommand{\LexicalBackground}{%
	\setbeamertemplate{background}{%
	\tikz[remember picture,overlay] {%
	\node[anchor=north west,inner sep=0mm,shift={(-1,0.5)}]
		at (current page.north west) {\includegraphics[scale=0.24]{letters-0}};
	}}}


\newcommand{\SyntacticBackground}{%
	\setbeamertemplate{background}{%
	\tikz[remember picture,overlay] {%
		\node[anchor=north west,inner sep=0mm,shift={(-1,0.5)}]
			at (current page.north west)%
		{\includegraphics[scale=0.8]{html-0.jpg}};
		}}}

\newcommand{\EcoreBackground}{%
	\setbeamertemplate{background}{%
	\tikz[remember picture,overlay] {%
		\node[anchor=north west,inner sep=0mm,shift={(-.3cm,2.3cm)},rotate=0]
		at (current page.north west)
		{\includegraphics[scale=0.43]{ecore-bg.png}};
		}}}


\newcommand{\BugBackground}{%
	\setbeamertemplate{background}{%
	\tikz[remember picture,overlay] {%
		\node[anchor=north east,inner sep=0mm] at (current page.north east)
		{\includegraphics[scale=0.8]{fig/bug-head-scaled.jpeg}};
		}}}


\newcommand{\BlackBackground}{%
  \setbeamertemplate{background}{}
  \setbeamercolor{background canvas}{bg=black}}
\newcommand{\WhiteBackground}{
  \setbeamertemplate{background}{}
  \setbeamercolor{background canvas}{bg=white}}
\newcommand{\EmptyBackground}{\WhiteBackground}

\makeatletter
\newcommand*{\overlaynumber}{\number\beamer@slideinframe}
\makeatother

\newcommand{\bkch}[1]{\hbox{$\!\!\mathtt\backslash$#1\!}}
\newcommand{\hI}[1]{\tikz[overlay]{\filldraw[SugarLove] (-0.05em,2ex) rectangle (#1,-0.8ex);}}


\newcommand{\sepa}[1]{\textcolor{DarkGreenGray}{#1}}
\newcommand{\jtyp}[1]{\textcolor{DarkGreenGray}{#1}}
\newcommand{\keyw}[1]{\texttt{\textbf{#1}}}
\newcommand{\comm}[1]{\textcolor{DarkGreenGray}{\emph{#1}}}
\newcommand{\anno}[1]{\textcolor{gray}{#1}}
\newcommand{\jstr}[1]{\texttt{\textcolor{DarkGreenGray}{"#1"}}}
\newcommand{\jjstr}[1]{\texttt{\textcolor{DarkGreenGray}{#1}}}

\newcommand{\oneCover}[1]{
	\BookBackground
	\begin{frame}[t]
	\begin{tikzpicture}
		\node[ThemeBox] %
		{\begin{minipage}{0.55\textwidth}
				\tikz\node[inner sep=0,blur shadow] {#1};
				%\strut\hfill\tikz\node[inner sep=0,blur shadow] {#2};
			\end{minipage}};
	\end{tikzpicture}
\end{frame}}


\newcommand{\twoCovers}[2]{
	\BookBackground
	\begin{frame}[t]
	\begin{tikzpicture}
		\node[ThemeBox] %
		{\begin{minipage}{0.95\textwidth}
				\tikz\node[inner sep=0,blur shadow] {#1};
				\strut\hfill\tikz\node[inner sep=0,blur shadow] {#2};
			\end{minipage}};
	\end{tikzpicture}
\end{frame}}


% gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.3 -dNOPAUSE -dQUIET -dBATCH -sOutputFile=01-109-13.pdf 01-109.pdf

\newcommand{\fourSlides}[5]{%
%	\AtriumBackground % better set manually at use point
\begin{frame}[t]
	\frametitle{#1}\framesubtitle{\strut}
	\vspace{-1mm}
	\begin{tikzpicture}[node distance=1ex]
		\node<2->[Framed,inner sep=0,blur shadow,fill=white] (s1) {#2};
		\node<3->[Framed,inner sep=0,blur shadow,fill=white] (s2) [right=of s1] {#3};
		\node<4->[Framed,inner sep=0,blur shadow,fill=white] (s3) [below=of s1] {#4};
		\node<5->[Framed,inner sep=0,blur shadow,fill=white] (s4) [right=of s3] {#5};
	\end{tikzpicture}
\end{frame}}

% this could be good to change to some foto frame, or film strip cheasy
% thingy
\newcommand{\oneSlide}[3][]{%
	\AtriumBackground
\begin{frame}[t]
	\frametitle{#2}
	\framesubtitle{#1}
	\begin{tikzpicture}
		\node[inner sep=0,blur shadow,fill=white,ClrLightDominant,ultra
		thin,anchor=north east,overlay] at (0.94\textwidth,0) {#3};
	\end{tikzpicture}
\end{frame}}

\newcommand{\threeSlides}[4]{%
	\AtriumBackground
\begin{frame}[t]
	\frametitle{#1}
	\vspace{-7mm}
	\begin{tikzpicture}[node distance=1ex]
		\node<2->[inner sep=0,blur shadow,fill=white] (s1) {#2};
		%\node<3->[inner sep=0,blur shadow,fill=white] (s2) [right=of s1] {#3};
		\node<3->[inner sep=0,blur shadow,fill=white] (s3) [below=of s1] {#3};
		\node<4->[inner sep=0,blur shadow,fill=white] (s4) [right=of s3] {#4};
	\end{tikzpicture}
\end{frame}}


\newcommand{\danger}%
	{\fontencoding{U}\fontfamily{futs}\selectfont\char 66\relax}

\newcommand{\colordemo}{{
    \MathBackground
  \begin{frame}[t]{Color Demo}
    \scriptsize
  \begin{tikzpicture}[node distance=1mm]
    \node[WhitePlaque] (zero) {WhitePlaque};
    \node[LightLightGreenPlaque] (one) [below=of zero.south west,anchor=north west] {LightLightGreenPlaque};
    \node[LightLightGreenGlass] (two) [right=8mm of one.north east,anchor=north west] {LightLightGreenGlass};
    \node[WhiteGlass] (half) [above=of two.north west,anchor=south west] {WhitePlaque};
    \node[MemoriesPlaque] (three) [below=of one.south west,anchor=north west] {MemoriesPlaque};
    \node[MemoriesGlass] (four) [below=of two.south west,anchor=north west] {MemoriesGlass};
    \node[DarkGreenGrayPlaque] (five) [below=of three.south west,anchor=north west] {DarkGreenGrayPlaque};
    \node[DarkGreenGrayGlass] (six) [below=of four.south west,anchor=north west] {DarkGreenGrayGlass};
    \node[RomanticPaleYellowPlaque] (seven) [below=of five.south west,anchor=north west] {RomanticPaleYellowPlaque};
    \node[RomanticPaleYellowGlass] (eight) [below=of six.south west,anchor=north west] {RomanticPaleYellowGlass};
    \node[BlackGlass] (nine) [below=of seven.south west,anchor=north west] {BlackGlass};
    \node[SugarLoveGlass] (ten) [below=of eight.south west,anchor=north west] {SugarLoveGlass};

    \node[LightLightGlass] (twenty) [right=10mm of two.north east, anchor=north west] {LightLightRedGlass};
    \node[LightLightPlaque] (twenty one) [right=10mm of
    twenty.north neast, anchor=north west] {LightLightPlaque};
    \node[MemoredsGlass] (twenty two) [below=of twenty.south west, anchor=north west] {MemoredsGlass};
    \node[MemoredsPlaque] (twenty three) [below=of twenty one.south west, anchor=north west] {MemoredsPlaque};
    \node[DarkGlass] (twenty four) [below=of twenty two.south west, anchor=north west] {DarkGlass};
    \node[DarkPlaque] (twenty five) [below=of twenty three.south west, anchor=north west] {DarkPlaque};
    \node[BlackGlass] (twenty six) [below=of twenty four.south west, anchor=north west] {BlackGlass};
    \node[BlackPlaque] (twenty seven) [below=of twenty five.south west, anchor=north west] {BlackPlaque};
  \end{tikzpicture}
\end{frame}}}

% change emphasis to bold in slides

\let\emph\relax % there's no \RedeclareTextFontCommand
\DeclareTextFontCommand{\emph}{\bfseries}

\setbeamerfont{alerted text}{series=\bfseries}
\setbeamercolor{alerted text}{fg=black}

% vim:tw=66:cc=66:nospell:syntax=tex:conceallevel=0

